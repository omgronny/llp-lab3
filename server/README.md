# LLP lab 1

## Installation and start

### Linux:

##### Requirements:
- cmake (3.21)
- make
- C++ compiler (Clang 14 or newer, gcc 10 or newer)

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab1.git
cd llp-lab1
mkdir build && cd build
cmake ..
make
./lab1_cpp
```

### Windows:

- cmake (3.21)
- make
- GCC 12.2.0 + MinGW-w64 10.0.0 (you can download it from [here](https://winlibs.com/))

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab1.git
cd llp-lab1
md build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - MinGW Makefiles" ..
cmake --build .
lab1_cpp.exe
```

## Description

### 1. Purposes

The purpose of a lab is to create a single-file data storage module of a software system

### 2. Problems

First I've come up with the structure of file -- How can I 

### 3. Solution description

There are 2 general modules in the system.

The first one is the user interface. It contains a query and result data structures.
It gives an interface to enter the command into the system. It also validates the data
(e.g type-checking)

The second one encapsulates file processing. There are 2 modules inside. One of them implements queries at the lower level. It processes the data, creates strings to store in
the file, and so on. And the last module implements low-level file work. It reads and writes data, invalidates deleted elements, finds table headers

You can see data structures in file `utils.h`

### 4. How do I store the data:

There are 2 main Lists: free-list which contains a list of freed places in the file
and a list of table headers

Every table header contains lists of data in a particular table. So we can iterate 
only by elements of one table

- If we want to insert elements in a table, we just find a needed table and then add an element (we check the free space and put a pointer to the element in the table header)

- If we want to select elements. we just iterate by the particular table and return every element using coroutines

- delete element is quite similar to select, but instead of returning element,
we remove it from the list and mark this place as free

## 5. Benchmarks

#### insert

```cpp
request: 
void insert_big(command_executor& commandExecutor) {
    for (int32_t i = 0; i < 10000; ++i) {
        commandExecutor.insert({"new_test4", {std::to_string(i), "str4" + std::to_string(i), "true"}});
    }
}
```

In the beginning, there are about 500'000 elements in the table "new_test4"

It's time of insert a 10'000 elements 20 times
```
496 433 434 411 410 433 432 441 415 607 479 508 427 608 437 417 413 606 480 486
```

![img.png](img.png)

- seems like O(1)

#### select

```sql
request: select * from new_test4 where col1 = 300;
```
Between every two `select`, 10'000 elements were added to the table

```
1399 1551 1542 1512 1563 1664 1793 1704 2086 2203 2200 1997 2079 2234 2136 2056 2139 2716 2878 2322
```


![img_1.png](img_1.png)

- seems like O(n)


#### delete time and file size

Test case: 

1) add 5000 random elements and 5000 elements with one attribute equals 300.

2) delete all elements with attribute equals 300 
3) do `1.` again

So in every step we add another 10'000 elements and delete 10'000 elements

```
file size: 572 953 1400 1700 2100 2500 2800 3200 3600 4000 4300
time: 80 179 195 248 245 296 320 313 372 353 380
```

Then, we delete all elements (1500 ms by the way) from table and do test again 

```
file size: 4300 4300 4300 4300 4300 4300 4300 4300 4300 4300
time: 100 175 199 228 242 263 292 311 330 353
```


As we can see, we reuse a free space in our file.

file size:

![img_2.png](img_2.png)

delete time:

![img_3.png](img_3.png)


### 6. Conclusion



