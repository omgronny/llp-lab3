
#ifndef LAB1_CPP_FILE_UTILS_H
#define LAB1_CPP_FILE_UTILS_H

#include <fcntl.h>
#include <cstring>
#include <sys/stat.h>
#include <string>
#include <csignal>
#include <vector>

#include "../utils.h"

// #{000000000000;000000000028}${000000000135;000000000000;new_test1;col1:INT,col2:STR,col3:BUL}

struct general_header {
    uint64_t free_list_ptr;
    uint64_t tables_headers_ptr;
};

struct theaders {
    uint64_t prev_ptr;
    uint64_t this_ptr;
    uint64_t last_ptr;
};

namespace file_utils {

    const uint16_t NUMBER_SIZE = 12;

    uint64_t write_into_db(int32_t fd, struct stat* m_stat_buf, int64_t offset, const char *data, size_t n);

    ssize_t read_from_db(int32_t fd, int64_t offset, char *data, size_t n);

    std::string number_to_8b_str(uint64_t num);

    uint64_t str_num_to_int(const std::string& str);

    uint64_t read_int_by_offset(int32_t fd, uint64_t offset);

    theaders find_theader_by_name(const std::string& table_name, int32_t fd, general_header gh,
                                  uint64_t line_size_constraint, const struct stat* m_stat_buf);

    uint64_t find_last_theader(int32_t fd, general_header gh);

    uint64_t find_free_place_by_size(int32_t fd, struct stat* stat_buf, uint64_t ptr_to_last_free_place, uint64_t needed_size);

    std::pair< std::vector<std::string>, std::vector<type> >
            get_columns_types(int32_t fd, uint64_t theader_offset,
                              uint64_t line_size_constraint, const struct stat* m_stat_buf);

    check_and_return_result check_this_element_and_create_return_str(int32_t fd,
                                                                          uint64_t this_el,
                                                                          char* line_buffer,
                                                                          int64_t columns_num,
                                                                          uint64_t line_size_constraint,
                                                                          compare_by cmp,
                                                                          type as_type,
                                                                          uint64_t column_number,
                                                                          const std::string& param,
                                                                          struct stat& stat_buf);

    bool invalidate_data(int32_t fd, struct stat* stat_buf, uint64_t offset, uint64_t columns_num,
                         uint64_t line_size_constraint);




}

#endif //LAB1_CPP_FILE_UTILS_H
