
#ifndef LAB1_CPP_FILE_CONSISTENCY_KEEPER_H
#define LAB1_CPP_FILE_CONSISTENCY_KEEPER_H

#include <iostream>
#include <algorithm>

#include <vector>

#include "../generator.hpp"

#include "file_utils.h"

/*
 * Этот модуль непосредственно следит за файлом
 * То есть делаем всю работу по слежению за файлом тут
*/

class file_consistency_keeper {

    const char* m_filename;
    uint64_t line_size_constraint;

    int32_t m_fd;
    struct stat m_stat_buf;
    general_header m_gh;

    general_header read_general_header() const;

public:

    /*
 * header:
 * #{free_list;th}
 * free-list - то указатель на конец! И в каждом ссылка на предыдущий
 * t_header:
 * ${th_next;last_data_ptr;table_name;col1:type1,col2:type2,col3:type3}
 *
 * data:
 * %{prev_ptr;f1;f2;f3}
 *
 * All ptrs is 8 bytes
 * #{00000000;00000020}${00000000;00000000;test;col1:INT,col2:STR,col3:INT}
 */

    explicit file_consistency_keeper(const char* filename, uint64_t line_size_constraint);

    bool new_table_create(const std::string& data);
    bool insert_in_table(const std::string& table_name, const std::string& data);

    generator select_or_delete_by_param(command_type commandType, const std::string& table_name, const std::string& column,
                                             compare_by cmp, type as_type, uint64_t column_number, const std::string& param);

    generator join_select(const join_select_command&, uint64_t, uint64_t);

    std::pair< std::vector<std::string>, std::vector<type> > get_columns_types(const std::string& table_name);

    bool drop_table(const std::string& table_name);

    /*
     * pointer = find(...)
     * Следим за фри листом
     * Проверяем что удалилось
     * и тд
     */

    ~file_consistency_keeper();

};


#endif //LAB1_CPP_FILE_CONSISTENCY_KEEPER_H
