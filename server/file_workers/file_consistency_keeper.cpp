#include "file_consistency_keeper.h"

/*
 * header:
 * #{free_list;th}
 *
 * t_header:
 * ${th_next;last_data_ptr;table_name;col1:type1,col2:type2,col3:type3}
 *
 * data:
 * %{prev_ptr;f1;f2;f3}
 *
 * All ptrs is NUMBER_SIZE bytes
 *
 */

file_consistency_keeper::file_consistency_keeper(const char* filename, uint64_t line_size_constraint)
    : m_filename(filename)
    , line_size_constraint(line_size_constraint){
    // тут нам нужно создать в файле основной хедер и все проинициализировать

    this->m_fd = open(filename, O_RDWR | O_CREAT);

    // std::cout << "construct: file descriptor = " << m_fd << std::endl;
    fstat(m_fd, &m_stat_buf);
    // std::cout << "construct: file size = " << m_stat_buf.st_size << std::endl;

    if (m_stat_buf.st_size == 0) {

        std::string begin = "#{000000000000;000000000028}${000000000000;000000000000;new_test1;col1:INT,col2:STR,col3:BUL}";

        file_utils::write_into_db(m_fd, &m_stat_buf, 0, begin.c_str(), begin.size());
    }

    m_gh = read_general_header();

    // std::cout << "construct: free_list_ptr" << m_gh.free_list_ptr << "construct: tables_headers_ptr" << m_gh.tables_headers_ptr << std::endl;

}

general_header file_consistency_keeper::read_general_header() const {

    size_t read_size = 2 + file_utils::NUMBER_SIZE + 1 + file_utils::NUMBER_SIZE + 1;

    char* read_data = new char [read_size + 1];
    read_data[read_size] = 0;
    file_utils::read_from_db(m_fd, 0, read_data, read_size);

    std::string s(read_data, read_size + 1);
    auto bs = s.substr(2, file_utils::NUMBER_SIZE);

    auto res1 = static_cast<uint64_t>(std::stoi(s.substr(2, file_utils::NUMBER_SIZE)));
    auto res2 = static_cast<uint64_t>(std::stoi(s.substr(2 + file_utils::NUMBER_SIZE + 1, file_utils::NUMBER_SIZE)));

    delete[] read_data;
    return {res1, res2 };
}

// принимаем что-то типо "test;col1:INT,col2:STR,col3:INT"
bool file_consistency_keeper::new_table_create(const std::string& data) {

    uint64_t last_theader_ptr = file_utils::find_last_theader(m_fd, m_gh);

    //std::cout << "new_table_create: last header = " << last_theader_ptr << std::endl;

    auto new_ptr = file_utils::number_to_8b_str(m_stat_buf.st_size);

    file_utils::write_into_db(m_fd, &m_stat_buf, last_theader_ptr + 2, new_ptr.c_str(), file_utils::NUMBER_SIZE);

//    std::cout << "new_table_create: next_ptr_begin = " << last_theader_ptr + 2
//        << " new_table_create: new_ptr = " << new_ptr << std::endl;

    size_t size_bytes = 2 + data.size() + 1 + file_utils::NUMBER_SIZE + 1 + file_utils::NUMBER_SIZE + 1;

    std::string str;
    str.reserve(size_bytes);

    str += "${" + file_utils::number_to_8b_str(0) + ";" + file_utils::number_to_8b_str(0) + ";";
    str += data;
    str += "}";

    file_utils::write_into_db(m_fd, &m_stat_buf, m_stat_buf.st_size, str.c_str(), size_bytes);

    return true;
}

/*
 * data:
 * %{prev_ptr;f1;f2;f3}
*/

/*
* записываем в конец файла новые данные
* в найденной таблице изменяем указатель на ласт на наш элемент
* (можно также сначала сохранить размер файла, чтобы не считать)
*/

/*
 * Нужно обратить внимание на то, что когда мы добавляем одним из полей в бд число,
 * мы не селдим за битностью, так как читаем его из файла как строку
 * поэтому нужно не воспользоваться случайно битностью как с указателями
 */

// принимаем что-то типо "test;f1;f2;f3"
bool file_consistency_keeper::insert_in_table(const std::string& table_name, const std::string& data) {
    // тут уже все проверено

    auto our_theader = file_utils::find_theader_by_name(table_name, m_fd, m_gh,
                                                        line_size_constraint, &m_stat_buf).this_ptr;

    uint64_t prev_for_this_el = file_utils::read_int_by_offset(m_fd, our_theader + 2 + file_utils::NUMBER_SIZE + 1);

    /*
     * найти во фри листе свободное место и писать туда
     */

    std::string str = "%{" + file_utils::number_to_8b_str(prev_for_this_el) + ";" + data + "}";

    // std::cout << "SIZE = " << str.size() << std::endl;

    uint64_t place = file_utils::find_free_place_by_size(m_fd, &m_stat_buf, file_utils::read_int_by_offset(m_fd, 2), str.size());

    uint64_t ptr = place != 0 ? place : m_stat_buf.st_size;
    file_utils::write_into_db(m_fd, &m_stat_buf, ptr, str.c_str(), str.size());

    file_utils::write_into_db(m_fd, &m_stat_buf, our_theader + 2 + file_utils::NUMBER_SIZE + 1,
                              file_utils::number_to_8b_str(ptr).c_str(), file_utils::NUMBER_SIZE);

    return false;
}

/*
     * В принципе можно возвращать офсет или типо того (то есть как-то искусственно задать
     * место откуда искать при следующем вызове)
     *
     * Так будет проще, и можно будет выгружать, например, пачками по 100
*/

/*
 * Нужно как-то понимать, последний это элемент или нет
 * если мы достаем все, то можно проверять просто на prev==0
 * но если нет, то сложнее
 *
 * Возможно нужно возвращать что-то типо option  и обрабатывать случай что ничего не нашлось
 */

/*
 * 1) find table
 * есть проблема, что мы это и так делаем в command_executor
 * но это не асимптотически плохо, поэтому может и норм
 *
 * 1.5) Достаем номер нужного столбца
 *
 * 2) берем ссылку на последний элемент и начинаем искать
 *
 * 3) Когда нашли элемент, возвращаем его, и нудно что-то типо генератора
 * Если дошли до последнего (у него prev == 0), то возвращаем None
 *
 */
generator file_consistency_keeper::select_or_delete_by_param(command_type commandType,
                                                                            const std::string &table_name,
                                                                            const std::string& column,
                                                                            const compare_by cmp,
                                                                            const type as_type,
                                                                            const uint64_t column_number,
                                                                            const std::string& param) {

    std::vector<std::string> res;

    auto our_theader = file_utils::find_theader_by_name(table_name, m_fd, m_gh,
                                                        line_size_constraint, &m_stat_buf).this_ptr;


    auto [columns, types] = file_utils::get_columns_types(m_fd, our_theader, line_size_constraint, &m_stat_buf);
    int64_t columns_num = types.size() + 1;

    char* line_buffer = new char [ (line_size_constraint + 1) * (columns_num + 1) ];

    //std::cout << "select_by_param: columns_num = " << columns_num << std::endl;

    /*
     * Так как у нас все последующие next_el указывают на начало
     * (т.е начало + 2 = офсет указателя на prev)
     * Тут мы делаем -2, чтобы при удалении последнего мы также сделали +2
     * и попали в нужный офсет
    */
    uint64_t next_el = (our_theader + 2 + file_utils::NUMBER_SIZE + 1) - 2;
    uint64_t this_el = file_utils::read_int_by_offset(m_fd, next_el + 2);

    while (this_el != 0) {

        uint64_t prev_ptr = file_utils::read_int_by_offset(m_fd, this_el + 2);

        // читаем данные
        auto [is_ok, this_res_full, this_value] = file_utils::check_this_element_and_create_return_str(
                m_fd, this_el, line_buffer, columns_num, line_size_constraint, cmp, as_type, column_number, param, m_stat_buf
                );

//        std::cout << "select_by_param: this_res_full = " << this_res_full << std::endl;
//        std::cout << "select_by_param: prev = " << prev_ptr << std::endl;
//        std::cout << "select_by_param: is_ok = " << is_ok << std::endl;

        if (is_ok) {

            switch (commandType) {
                case command_type::SELECT: {
                    co_yield this_res_full;
                    next_el = this_el;
                    break;
                }

                case command_type::DELETE: {
                    // next.prev = prev
                    file_utils::write_into_db(m_fd, &m_stat_buf, next_el + 2,
                                              file_utils::number_to_8b_str(prev_ptr).c_str(), file_utils::NUMBER_SIZE);

                    // invalidate this_el and add to free list
                    file_utils::invalidate_data(m_fd, &m_stat_buf, this_el, columns_num, line_size_constraint);
                    break;
                }

            }

        } else {
            next_el = this_el;
        }

        this_el = prev_ptr;
    }

    delete[] line_buffer;
    //return res;
}

generator
file_consistency_keeper::join_select(const join_select_command& jsc, const uint64_t column_number_first,
                                     const uint64_t column_number_second) {

    std::vector<std::vector<std::string>> res;
    auto our_theader = file_utils::find_theader_by_name(jsc.table_name, m_fd, m_gh,
                                                        line_size_constraint, &m_stat_buf).this_ptr;

    char* line_buffer = new char [ (line_size_constraint + 1) * (std::max(column_number_first, column_number_second) + 1) ];

    auto [columns, types] = file_utils::get_columns_types(m_fd, our_theader, line_size_constraint, &m_stat_buf);
    int64_t columns_num = types.size() + 1;

    uint64_t next_el = (our_theader + 2 + file_utils::NUMBER_SIZE + 1) - 2;
    uint64_t this_el = file_utils::read_int_by_offset(m_fd, next_el + 2);

    while (this_el != 0) {

        uint64_t prev_ptr = file_utils::read_int_by_offset(m_fd, this_el + 2);

        auto [is_ok, this_res_full, this_value_in_first_table] = file_utils::check_this_element_and_create_return_str(
                m_fd, this_el, line_buffer, columns_num, line_size_constraint, NO_COMPARE, STR,
                column_number_first, "", m_stat_buf
        );

        if (is_ok) {
            // селектим только те, у кого поле совпадает с нашим
            auto res_for_this_element = select_or_delete_by_param(command_type::SELECT,
                                                                  jsc.second_table_name, jsc.second_column,
                                                                  EQUAL, STR,
                                                                  column_number_second, this_value_in_first_table);

            for (auto ref : res_for_this_element) {
                auto rs = ref + this_value_in_first_table;
                co_yield rs;
            }

            //res.emplace_back(res_for_this_element);

        }

        next_el = this_el;
        this_el = prev_ptr;
    }

    //return res;
}

std::pair<std::vector<std::string>, std::vector<type> > file_consistency_keeper::get_columns_types(const std::string& table_name) {

    uint64_t theader = file_utils::find_theader_by_name(table_name, m_fd, m_gh, line_size_constraint, &m_stat_buf).this_ptr;
    return file_utils::get_columns_types(m_fd, theader, line_size_constraint, &m_stat_buf);

}

bool file_consistency_keeper::drop_table(const std::string &table_name) {

    // это указатели на хедеры, а не нексты
    auto [prev_ptr, this_ptr, next_ptr] = file_utils::find_theader_by_name(table_name, m_fd,
                                                         m_gh, line_size_constraint,&m_stat_buf);

    // prev.next = next
    uint64_t next_number = file_utils::read_int_by_offset(m_fd, this_ptr + 2);
    auto ptr_for_write = file_utils::number_to_8b_str(next_number);
    file_utils::write_into_db(m_fd, &m_stat_buf, prev_ptr + 2, ptr_for_write.c_str(), file_utils::NUMBER_SIZE);

    return false;
}

file_consistency_keeper::~file_consistency_keeper() {
    close(this->m_fd);
}














