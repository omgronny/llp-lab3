#include <iostream>
#include "file_utils.h"

namespace file_utils {

    uint64_t write_into_db(int32_t fd, struct stat* m_stat_buf, const int64_t offset, const char *data, const size_t n) {

        auto offset1 = lseek(fd, offset, SEEK_SET);
        write(fd, data, n);
        fstat(fd, m_stat_buf);
        return offset1;

    }

    ssize_t read_from_db(int32_t fd, const int64_t offset, char *data, const size_t n) {
        lseek(fd, offset, SEEK_SET);
        return read(fd, data, n);
    }

    std::string number_to_8b_str(uint64_t num) {
        std::string res;
        auto s = std::to_string(num);

        for (int8_t i = 0; i < NUMBER_SIZE - (int8_t) s.size(); ++i) {
            res += "0";
        }
        res += s;

        return res;
    }

    uint64_t str_num_to_int(const std::string& str) {
        return static_cast<uint64_t>(std::stoi(str));
    }

    char* num_buffer = new char [ NUMBER_SIZE ];
    uint64_t read_int_by_offset(int32_t fd, uint64_t offset) {

        file_utils::read_from_db(fd, offset, num_buffer, NUMBER_SIZE);
        std::string str(num_buffer, NUMBER_SIZE);

        return file_utils::str_num_to_int(str);

    }

    theaders find_theader_by_name(const std::string& table_name, const int32_t fd, const general_header gh,
                                  const uint64_t line_size_constraint, const struct stat* m_stat_buf) {

        uint64_t table_ptr = gh.tables_headers_ptr;
        uint64_t next_table;
        uint64_t prev_table = 0;

        if (table_ptr == 0) {
            return {0, 0, 0};
        }

        char* str_buffer = new char [ line_size_constraint ];
        do {
            // table_ptr это указатель на сам хедер, а не на некст

            uint64_t table_name_ptr = table_ptr + 2 + NUMBER_SIZE + 1 + NUMBER_SIZE + 1;
            uint64_t read_size = m_stat_buf->st_size - table_name_ptr;

            // тут можно не читать несколько раз так мы вытаскиваем только одно поле
            read_from_db(fd, table_name_ptr, str_buffer, std::min(read_size, line_size_constraint));

            std::string read_string(str_buffer, line_size_constraint);

            uint64_t end_table_name_idx = read_string.find(';', 0);

            read_from_db(fd, table_ptr + 2, num_buffer, NUMBER_SIZE);
            std::string num_str(num_buffer, NUMBER_SIZE);

            next_table = file_utils::str_num_to_int(num_str);

            if (table_name == read_string.substr(0, end_table_name_idx)) {
                return {prev_table, table_ptr, next_table} ;
            }

            prev_table = table_ptr;
            table_ptr = next_table;

        } while (next_table != 0);

        delete[] str_buffer;
        return {prev_table, table_ptr, next_table} ;
    }

    uint64_t find_last_theader(int32_t fd, general_header gh) {

        uint64_t table_ptr = gh.tables_headers_ptr;
        uint64_t next_table;

        if (table_ptr == 0) {
            // TODO: случай когда 0 таблиц
        }

        do {
            // table_ptr это указатель на сам хедер, а не на некст

            read_from_db(fd, table_ptr + 2, num_buffer, NUMBER_SIZE);
            std::string num_str(num_buffer, NUMBER_SIZE);

            next_table = file_utils::str_num_to_int(num_str);

            if (next_table != 0) {
                table_ptr = next_table;
            }

        } while (next_table != 0);

        return table_ptr;
    }

    std::pair< std::vector<std::string>, std::vector<type> >
            get_columns_types(int32_t fd, uint64_t theader_offset,
                              uint64_t line_size_constraint, const struct stat* m_stat_buf) {

        std::vector<type> res_types;
        std::vector<std::string> res_columns;

        char* line_buffer = new char [ line_size_constraint ];

        int64_t columns_num = 0;
        uint64_t columns_begin = theader_offset + 2 + NUMBER_SIZE + 1 + NUMBER_SIZE + 1;

        uint64_t read_size = m_stat_buf->st_size - columns_begin;

        file_utils::read_from_db(fd, columns_begin, line_buffer, std::min(read_size, line_size_constraint));
        std::string line_buffer_str(line_buffer, line_size_constraint);

        columns_begin += line_buffer_str.find(';', 1) + 1;
        read_size = m_stat_buf->st_size - columns_begin;

        while (1) {

            file_utils::read_from_db(fd, columns_begin, line_buffer, std::min(read_size, line_size_constraint));
            std::string column_buffer_str(line_buffer, line_size_constraint);

            auto found = column_buffer_str.find(':', 1);
            auto maybe_next_column = found + 1 + 3;

            if (found == std::string::npos || column_buffer_str.find('}', 1) < found) {
                break;
            }

            columns_num++;
            columns_begin += maybe_next_column + 1;

            auto type = column_buffer_str.substr(found + 1, 3);
            if (type == "INT") {
                res_types.push_back(INT);
            } else if (type == "FLT") {
                res_types.push_back(FLT);
            } else if (type == "BUL") {
                res_types.push_back(BUL);
            } else {
                res_types.push_back(STR);
            }

            res_columns.push_back(column_buffer_str.substr(0, found));

            if (column_buffer_str[maybe_next_column] == '}') {
                break;
            }

        }

        delete[] line_buffer;
        return { res_columns, res_types };

    }


    check_and_return_result check_this_element_and_create_return_str(int32_t fd,
                                                                          uint64_t this_el,
                                                                          char* line_buffer,
                                                                          const int64_t columns_num,
                                                                          const uint64_t line_size_constraint,
                                                                          const compare_by cmp,
                                                                          const type as_type,
                                                                          const uint64_t column_number,
                                                                          const std::string& param,
                                                                          struct stat& stat_buf) {

        bool is_ok = cmp == NO_COMPARE;

        uint64_t data_begin = this_el + 2 + NUMBER_SIZE + 1;

        // std::cout << "check_this_element_and_create_return_str: column_number = " << column_number << std::endl;

        uint64_t read_size = stat_buf.st_size - data_begin;

        file_utils::read_from_db(fd, data_begin, line_buffer,
                                 std::min(read_size, (line_size_constraint + 1) * (columns_num + 1)));
        std::string extended_value(line_buffer, line_size_constraint);

//        std::cout << std::endl;
//        std::cout << extended_value << std::endl;
//        std::cout << std::endl;

        // std::string this_res;
        std::string this_string;

        // this_res.reserve(line_size_constraint + 1);

        std::string this_res = extended_value.substr(0, extended_value.find('}', 0));

        uint64_t this_value_idx = 0;
        for (uint64_t i = 0; i < columns_num; ++i) {

            uint64_t end_value_pos = extended_value.find(';', this_value_idx);
            uint64_t end_row_pos = extended_value.find('}', this_value_idx);

            std::string value = extended_value.substr(this_value_idx, std::min(end_value_pos, end_row_pos) - this_value_idx);

            //std::cout << "value = " << value << " idx = " << this_value_idx << " end_value_pos = " << end_value_pos << std::endl;

            this_value_idx = std::min(end_value_pos, end_row_pos) + 1;

            // мы уже все проверили при добавлении или при самом селекте, поэтому никаких проверок корректности не надо
            if (cmp != NO_COMPARE && column_number == i - 1 ) {

                if (as_type == INT || as_type == FLT) {

                    switch (cmp) {

                        case GREATER:
                            is_ok = file_utils::str_num_to_int(value) > file_utils::str_num_to_int(param);
                            break;
                        case GREATER_OR_EQUAL:
                            is_ok = file_utils::str_num_to_int(value) >= file_utils::str_num_to_int(param);
                            break;
                        case LESS:
                            is_ok = file_utils::str_num_to_int(value) < file_utils::str_num_to_int(param);
                            break;
                        case LESS_OR_EQUAL:
                            is_ok = file_utils::str_num_to_int(value) <= file_utils::str_num_to_int(param);
                            break;
                        case EQUAL:
                            is_ok = file_utils::str_num_to_int(value) == file_utils::str_num_to_int(param);
                            break;
                        case NOT_EQUAL:
                            is_ok = file_utils::str_num_to_int(value) != file_utils::str_num_to_int(param);
                            break;

                    }

                } else {

                    switch (cmp) {

                        case EQUAL:
                            is_ok = value == param;
                            break;
                        case NOT_EQUAL:
                            is_ok = value != param;
                            break;
                        case GREATER:
                        case GREATER_OR_EQUAL:
                        case LESS:
                        case LESS_OR_EQUAL:
                        case NO_COMPARE:
                            break;
                    }

                }

            }

            if (column_number == i - 1) {
                this_string = value;
            }

            //this_res += value + "; ";
            data_begin += end_value_pos + 1;

        }

        return { is_ok, this_res, this_string };
    }

    // %{ptr;f1;f2;f3} -> !{ptr_prev;size}...
    bool invalidate_data(const int32_t fd, struct stat* stat_buf, const uint64_t offset, const uint64_t columns_num,
            const uint64_t line_size_constraint) {

        size_t data_size = std::min((line_size_constraint + 1) * (columns_num + 1),  stat_buf->st_size - offset);
        char* str_buffer = new char [ data_size ];

        // вычисляем размер данных
        read_from_db(fd, offset, str_buffer, data_size);
        std::string data_str(str_buffer, data_size);

        auto end = data_str.find('}', 0);

        // добавляем во фри лист
        uint64_t old_first = read_int_by_offset(fd, 2);
        std::string new_first_str = number_to_8b_str(offset);
        write_into_db(fd, stat_buf, 2, new_first_str.c_str(), NUMBER_SIZE);

        // записываем
        std::string res = "!{"; // " %{ptr; "
        res += number_to_8b_str(old_first) + ";";
        res += number_to_8b_str(end + 1) + "}"; // TODO можно ли это убрать?

        write_into_db(fd, stat_buf, offset, res.c_str(), res.size());

        return true;
    }


    uint64_t find_free_place_by_size(const int32_t fd, struct stat* stat_buf, const uint64_t ptr_to_last_free_place, const uint64_t needed_size) {

        uint64_t prev_place = 0;
        uint64_t this_place = ptr_to_last_free_place;

        while (this_place != 0) {

            uint64_t this_size = read_int_by_offset(fd, this_place + 2 + NUMBER_SIZE + 1);

            if (this_size >= needed_size) {

                // prev.next = this.next
                read_from_db(fd, this_place + 2, num_buffer, NUMBER_SIZE);
                write_into_db(fd, stat_buf, prev_place + 2, num_buffer, NUMBER_SIZE);

                return this_place;
            }

            prev_place = this_place;
            this_place = read_int_by_offset(fd, this_place + 2);
        }

        return 0;
    }

}