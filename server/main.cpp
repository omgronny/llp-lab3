#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/write.hpp>

#include <cstdio>
#include <coroutine>
#include <iostream>

#include "commands/command_executor.h"
#include "test/test.hpp"
#include "eval.h"

#include "hello.hxx"

using boost::asio::ip::tcp;
using boost::asio::awaitable;
using boost::asio::co_spawn;
using boost::asio::detached;
using boost::asio::use_awaitable;
namespace this_coro = boost::asio::this_coro;

#if defined(BOOST_ASIO_ENABLE_HANDLER_TRACKING)
# define use_awaitable \
  boost::asio::use_awaitable_t(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#endif


awaitable<void> request(tcp::socket& socket, command_executor& commandExecutor) {

    char data[1024 * 100];

    std::size_t n = co_await socket.async_read_some(boost::asio::buffer(data), use_awaitable);
    data[n] = 0;
    std::string str(data);
    str.resize(n);

    auto res = eval::evaluate(str, commandExecutor);
    co_await async_write(socket, boost::asio::buffer(res.c_str(), res.size()), use_awaitable);

}

awaitable<void> accept_new_client(tcp::socket socket, command_executor& commandExecutor) {
    std::cout << "new client" << std::endl;
    try {

        for (;;) {

            co_await request(socket, commandExecutor);

        }

    } catch (std::exception& e) {
        std::printf("echo Exception: %s\n", e.what());
    }

}

awaitable<void> listener(command_executor& commandExecutor) {

    auto executor = co_await this_coro::executor;
    tcp::acceptor acceptor(executor, {tcp::v4(), 8187});

    std::cout << "listener" << std::endl;

    for (;;) {

        tcp::socket socket = co_await acceptor.async_accept(use_awaitable);
        co_spawn(executor, accept_new_client(std::move(socket), commandExecutor), detached);

    }
}

int main() {

    command_executor commandExecutor("../../server/db.txt", 50);

    try {

        boost::asio::io_context io_context(1);

        boost::asio::signal_set signals(io_context, SIGINT, SIGTERM);
        signals.async_wait([&](auto, auto){ io_context.stop(); });

        co_spawn(io_context, listener(commandExecutor), detached);

        io_context.run();

    } catch (std::exception& e) {
        std::printf("Exception: %s\n", e.what());
    }
}


//int main() {
//
//    command_executor commandExecutor("../../server/db.txt", 50);
//
//    // в цикле принимаем команды, разбираем их
//    // формируем структуры
//    // выполняем запрос - возвращаем ответ
//
//
//
////    create(commandExecutor);
////
////    insert_simple(commandExecutor);
////
////    //insert_big(commandExecutor);
////
////    select(commandExecutor);
////
////    //_delete(commandExecutor);
////
////    //stress_select(commandExecutor);
////
////    //stress_insert_and_select(commandExecutor);
////    //stress_insert_and_update(commandExecutor);
////
////    //stress_delete_and_file_size(commandExecutor);
////
////    //test_float(commandExecutor);
//
//    return 0;
//}




