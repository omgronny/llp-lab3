#pragma once
#include "../commands/command_executor.h"

// #{000000000000;000000000028}${000000000000;000000000000;new_test1;col1:INT,col2:STR,col3:BUL}
void create(command_executor& commandExecutor) {

    commandExecutor.create({
                                   "new_test2",
                                   {{"col1", INT}, {"col2", STR}}
                           });
    commandExecutor.create({
                                   "new_test3",
                                   {{"col3", BUL}}
                           });
    commandExecutor.create({
                                   "new_test4",
                                   {{"col1", INT}, {"col2", STR}, {"col3", BUL}}
                           });

}

void insert_simple(command_executor& commandExecutor) {
    commandExecutor.insert({ "new_test2", {"228", "test_string_last"} });
    commandExecutor.insert({ "new_test2", {"228", "test_string2222"} });

    commandExecutor.insert({"new_test2", {"15005", "test_string2"} });
    commandExecutor.insert({"new_test3", {"false"} });
}

void insert_big(command_executor& commandExecutor) {
    for (int32_t i = 0; i < 10000; ++i) {
        commandExecutor.insert({"new_test4", {std::to_string(i), "str4" + std::to_string(i), "true"}});
    }
}

void select(command_executor& commandExecutor) {

//    auto res = commandExecutor.select({ "new_test2", "col1", NO_COMPARE, "", STR });
//
//    std::cout << std::endl;
//    std::cout << "select * from new_test2;" << std::endl;
//    for (const auto & rs : res.rows) {
//        std::cout << rs << std::endl;
//    }

//    auto res1 = commandExecutor.select(
//            {"new_test4", "col1", EQUAL, "300", INT}
//    );
//
//    std::cout << std::endl;
//    std::cout << "select * from new_test4 where col1 = 300;" << std::endl;
//    for (const auto &rs: res1.rows) {
//        std::cout << rs << std::endl;
//    }

    auto res2 = commandExecutor.select({"new_test4", "col1", LESS, "5", INT});

    std::cout << std::endl;
    std::cout << "select * from new_test4 where col1 < 5;" << std::endl;
    for (const auto &rs: res2.rows) {
        std::cout << rs << std::endl;
    }
}


void _delete(command_executor& commandExecutor) {

    std::cout << "delete * from new_test2 where col1 = 228;" << std::endl;
    commandExecutor._delete({"new_test2", "col1", EQUAL, "228", INT});


    auto res = commandExecutor.select({
                                              "new_test2", "col1", EQUAL, "228", INT
                                      });

    std::cout << std::endl;
    std::cout << "select * from new_test2 where col1 = 228;" << std::endl;
    for (const auto &rs: res.rows) {
        std::cout << rs << std::endl;
    }

}

void update(command_executor& commandExecutor) {
    commandExecutor.update({ "new_test4", "col1", EQUAL, "228", INT, {"42", "str4opt", "false"} });
}

void stress_select(command_executor& commandExecutor) {

    std::vector<int32_t> select_time;

    for (int32_t i = 0; i < 10; ++i) {

        auto tStart = clock();

        select(commandExecutor);

        auto tm = ((double) clock() - tStart) / CLOCKS_PER_SEC * 1000;
        select_time.push_back(tm);

    }

    for (auto & ut : select_time) {
        std::cout << ut << " ";
    }
    std::cout << std::endl;

}

void stress_insert_and_select(command_executor& commandExecutor) {

    std::vector<int32_t> insert_time;
    std::vector<int32_t> select_time;

    for (int32_t i = 0; i < 10; ++i) {

        {
            clock_t tStart = clock();

            insert_big(commandExecutor);

            clock_t tm = ((double) clock() - tStart) / CLOCKS_PER_SEC * 1000;
            insert_time.push_back(tm);
        }

        {
            clock_t tStart = clock();

            select(commandExecutor);

            clock_t tm = ((double) clock() - tStart) / CLOCKS_PER_SEC * 1000;
            select_time.push_back(tm);
        }

    }

    for (auto & ut : insert_time) {
        std::cout << ut << " ";
    }
    std::cout << std::endl;

    for (auto & ut : select_time) {
        std::cout << ut << " ";
    }
    std::cout << std::endl;

}


void stress_insert_and_update(command_executor& commandExecutor) {

    std::vector<int32_t> update_time;

    for (int32_t i = 0; i < 10; ++i) {

        insert_big(commandExecutor);

        clock_t tStart = clock();

        update(commandExecutor);

        clock_t tm = ((double) clock() - tStart) / CLOCKS_PER_SEC * 1000;
        update_time.push_back(tm);

    }

    for (auto & ut : update_time) {
        std::cout << ut << " " << std::endl;
    }
    std::cout << std::endl;

}

void select_300_from_test2(command_executor& commandExecutor) {
    auto res1 = commandExecutor.select(
            {"new_test4", "col1", EQUAL, "300", INT}
    );

    std::cout << std::endl;
    std::cout << "select * from new_test4 where col1 = 300;" << std::endl;
    for (const auto &rs: res1.rows) {
        std::cout << rs << std::endl;
    }
}

void stress_delete_and_file_size(command_executor& commandExecutor) {

    // create(commandExecutor);

    std::vector<int32_t> delete_time;

    for (int32_t j = 0; j < 10; ++j) {

        for (int32_t i = 0; i < 5'000; ++i) {
            commandExecutor.insert({"new_test4", {"200", "str4", "true"}});
            commandExecutor.insert({"new_test4", {"300", "str4", "true"}});
        }

        clock_t tStart = clock();

        commandExecutor._delete({"new_test4", "col1", EQUAL, "300", INT});

        clock_t tm = ((double) clock() - tStart) / CLOCKS_PER_SEC * 1000;

        delete_time.push_back(tm);

        for (int32_t i = 0; i < 5'000; ++i) {
            commandExecutor.insert({"new_test4", {"200", "str4", "true"}});
            commandExecutor.insert({"new_test4", {"300", "str4", "true"}});
        }

    }

    for (auto & ut : delete_time) {
        std::cout << ut << " " << std::endl;
    }
    std::cout << std::endl;

}

void test_float(command_executor& commandExecutor) {
    // create table float_table(col0: str, col1: float, col2: float)

//    commandExecutor.create({
//                                   "float_table",
//                                   {{"col1", STR}, {"col2", FLT}, {"col3", FLT}}
//                           });

//    // insert smth
    commandExecutor.insert({"float_table", {"test", "25.5", "30.228"}});
    commandExecutor.insert({"float_table", {"test", "292929229", "1"}});
    commandExecutor.insert({"float_table", {"test", "378.909", "2.2"}});
    commandExecutor.insert({"float_table", {"test", "23.093", "3.45"}});
    commandExecutor.insert({"float_table", {"test", "25.5", "37898"}});
//
//    // select * from float_table
//
    auto res1 = commandExecutor.select(
            {"float_table", "col1", NO_COMPARE, "", STR}
    );

    std::cout << std::endl;
    std::cout << "select * from float_table;" << std::endl;
    for (const auto &rs: res1.rows) {
        std::cout << rs << std::endl;
    }
}

