#include <iostream>
#include "command_executor.h"

command_executor::command_executor(const char* filename, uint64_t line_size_constraint)
    : fck(filename, line_size_constraint)
    , line_size_constraint(line_size_constraint) {
}

std::optional<std::pair<std::vector<std::string>, uint64_t>> command_executor::type_check(const std::string& table_name,
                                                                     const std::string& column,
                                                                     const type as_type,
                                                                     const compare_by cmp) {

    auto [columns, types] = fck.get_columns_types(table_name);
    uint64_t col_num = 0;

    if (cmp == NO_COMPARE) {
        return std::make_pair(columns, col_num);
    }

    bool is_column_exist;
    for (int i = 0; i < columns.size(); ++i) {

        if (column == columns[i]) {

            is_column_exist = true;
            col_num = i;

            if ((as_type == type::INT && types[i] != INT) ||
                (as_type == type::BUL && types[i] != BUL) ||
                (as_type == type::FLT && types[i] != FLT)) {
                is_column_exist = false;
                break;
            }
        }
    }

    if (as_type != INT && as_type != FLT && cmp != NOT_EQUAL && cmp != EQUAL && cmp != NO_COMPARE) {
        std::cout << "bad comparator type" << std::endl;
        return std::nullopt;
    }

    if (!is_column_exist) {
        std::cout << "bad column or type" << std::endl;
        return std::nullopt;
    }

    return std::make_pair(columns, col_num);
}

// отправляем что-то типо "test;f1;f2;f3"
bool command_executor::insert(const insert_command& insertCommand) {

    auto [columns, types] = fck.get_columns_types(insertCommand.table_name);

    if (insertCommand.columns.size() != columns.size()) {
        std::cout << "bad columns number" << std::endl;
        return false;
    }

    std::string res = insertCommand.table_name + ";";
    for (uint64_t i = 0; i < insertCommand.columns.size(); ++i) {

        if (types[i] == INT) {
            bool is_digit = !insertCommand.columns[i].empty()
                            && std::find_if(insertCommand.columns[i].begin(), insertCommand.columns[i].end(),
                                            [](unsigned char c) {
                                                return !std::isdigit(c);
                                            }) == insertCommand.columns[i].end();
            if (!is_digit) {
                std::cout << "value is not int" << std::endl;
                return false;
            }
        } else if (types[i] == FLT) {
            char* check;
            double flt_number = std::strtod(insertCommand.columns[i].c_str(), &check);
            if (flt_number == 0 && check == insertCommand.columns[i].c_str()) {
                std::cout << "value is not float" << std::endl;
                return false;
            }
        } else if (types[i] == BUL && insertCommand.columns[i] != "true" && insertCommand.columns[i] != "false") {
            std::cout << "value is not bool" << std::endl;
            return false;
        }

        res += insertCommand.columns[i] + ";";
    }
    res.pop_back();

    return fck.insert_in_table(insertCommand.table_name, res);
}

returned_data command_executor::select(const select_command& sc) {

    auto maybe_col_num = type_check(sc.table_name, sc.column, sc.as_type, sc.cmp);
    uint64_t col_num;

    if (maybe_col_num.has_value()) {
        col_num = maybe_col_num.value().second;
    } else {
        return {};
    }

    returned_data res;
    for (auto element : fck.select_or_delete_by_param(command_type::SELECT, sc.table_name, sc.column, sc.cmp, sc.as_type, col_num, sc.param)) {
        res.rows.push_back(element);
    }

    return res;
}

returned_data command_executor::join_select(const join_select_command & jsc) {

    auto maybe_columns_fst = type_check(jsc.table_name, jsc.first_column, type::STR, NO_COMPARE);
    auto maybe_columns_snd = type_check(jsc.second_table_name, jsc.second_column, type::STR, NO_COMPARE);

    if (!maybe_columns_fst.has_value() || !maybe_columns_snd.has_value()) {
        return {};
    }

    bool join_column_exist = false;
    for (const auto & column : maybe_columns_fst.value().first) {
        if (jsc.first_column == column) {
            join_column_exist = true;
            break;
        }
    }

    if (!join_column_exist) {
        std::cout << "bad join: column not found" << std::endl;
    }

    join_column_exist = false;
    for (const auto & column : maybe_columns_fst.value().first) {
        if (jsc.first_column == column) {
            join_column_exist = true;
            break;
        }
    }

    if (!join_column_exist) {
        std::cout << "bad join: column not found" << std::endl;
    }

    returned_data res;
    for (auto element : fck.join_select(jsc, maybe_columns_fst.value().second, maybe_columns_snd.value().second)) {
        res.rows.push_back(element);
    }
    return res;

}

bool command_executor::update(const update_command& updateCommand) {

    // delete + insert
    _delete(updateCommand);
    insert(updateCommand);
    return false;
}

bool command_executor::_delete(const delete_command& dc) {

    // нашли -> удалили (можно по одному)
    auto maybe_col_num = type_check(dc.table_name, dc.column, dc.as_type, dc.cmp);
    uint64_t col_num;

    if (maybe_col_num.has_value()) {
        col_num = maybe_col_num.value().second;
    } else {
        return false;
    }

    fck.select_or_delete_by_param(command_type::DELETE, dc.table_name, dc.column,
                                  dc.cmp, dc.as_type, col_num, dc.param);

    return true;
}

// отправить что-то типо "test;col1:INT,col2:STR,col3:INT"
bool command_executor::create(const create_command& createCommand) {

    if (createCommand.table_name.size() >= line_size_constraint) {
        std::cout << "table name too long" << std::endl;
        return false;
    }
    std::string res = createCommand.table_name + ";";

    for (const auto & [name, type] : createCommand.columns) {
        if (name.size() > line_size_constraint) {
            std::cout << "name of column are too long" << std::endl;
            return false;
        }
        res += name + ":" + to_string(type) + ",";
    }
    res.pop_back();

    return fck.new_table_create(res);
}

bool command_executor::drop(const drop_command&) {
    return false;
}

std::string command_executor::to_string(type tp) {
    switch (tp) {
        case INT: return "INT";
        case STR: return "STR";
        case BUL: return "BUL";
        case FLT: return "FLT";
    }
}




