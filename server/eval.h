#pragma once

#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

#include "utils.h"
#include "command.hxx"

namespace eval {

    namespace {

        std::string clean(const std::string &input) {
            std::string res;
            res.reserve(input.size());

            for (const auto &chr: input) {
                if (!std::isspace(chr)) {
                    res.push_back(chr);
                }
            }

            return res;
        }

        type type_from_str(std::string &str) {
            if (str == "BOOl") {
                return BUL;
            } else if (str == "FLOAT") {
                return FLT;
            } else if (str == "STR") {
                return STR;
            } else if (str == "INT") {
                return INT;
            }
        }

        compare_by compare_from_str(std::string &str) {
            if (str == "GREATER") {
                return GREATER;
            } else if (str == "LESS") {
                return LESS;
            } else if (str == "GREATER_OR_EQUAL") {
                return GREATER_OR_EQUAL;
            } else if (str == "LESS_OR_EQUAL") {
                return LESS_OR_EQUAL;
            } else if (str == "NOT_EQUAL") {
                return NOT_EQUAL;
            } else if (str == "EQUAL") {
                return EQUAL;
            } else {
                return NO_COMPARE;
            }
        }

    }


    namespace {

        std::string base_select_or_delete_evaluate(const boost::property_tree::ptree &pt, command_executor& ce,
                                                   bool select) {

            auto name = pt.get<std::string>("table");
            auto list = pt.get_child("cmp"); // error if filter

            if (list.get<std::string>("nullable", "") == "nullptr") {
                std::cout << name << std::endl;
                if (select) {
                    std::string res;
                    auto selected = ce.select({name, "", NO_COMPARE, "", STR});
                    for (const auto &item: selected.rows) {
                        res += item + "\n";
                    }
                    return !res.empty() ? res : "empty";
                } else {
                    return "deleted";
                }
            }

            list = list.get_child("compare");

            auto column = list.get<std::string>("left_operand"); // .get<std::string>("column_name");
            auto cmp = list.get<std::string>("compare_by");
            auto value = list.get<std::string>("right_operand");

            type as_type = STR;
            auto this_type = list.get<std::string>("right_operand.<xmlattr>.type");
            if (this_type == "number") {
                as_type = INT;
            } else if (this_type == "string") {
                as_type = STR;
            } else if (this_type == "bool") {
                as_type = BUL;
            } else if (this_type == "float") {
                as_type = FLT;
            }

            if (select && pt.get_child("join").get<std::string>("nullable", "") != "nullptr") {
                auto second_name = pt.get<std::string>("join.select.table");
                std::string res;
                std::cout << name << " " << second_name << " " << column << " " << value << std::endl;
                auto selected = ce.join_select({name, second_name, column, value });
                for (const auto &item: selected.rows) {
                    res += item + "\n";
                }
                return res;
            }

            std::cout << name << " " << column << " " << compare_from_str(cmp) << " " << value << std::endl;

            if (select) {
                std::string res;
                auto selected = ce.select({ name, column, compare_from_str(cmp), value, as_type });
                for (const auto &item: selected.rows) {
                    res += item + "\n";
                }
                return !res.empty() ? res : "empty";
            } else {
                if (ce._delete({ name, column, compare_from_str(cmp), value, as_type })) {
                    return "deleted";
                } else {
                    return "error in delete: wrong query";
                }
            }
        }

        std::string insert_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {

            auto name = pt.get<std::string>("name");
            auto list = pt.get_child("list");

            std::vector<std::string> values;
            BOOST_FOREACH(const auto &v, list) {
                std::string str;
                            BOOST_FOREACH(const auto &u, v.second) {
                                            auto this_tp = u.second.get<std::string>("<xmlattr>.type");
                                            if (this_tp != "column_name") {
                                                str = u.second.get<std::string>("");
                                            }
                                        }
                values.push_back(str); // values.push_back(v.second.get<std::string>("value"));

                        }
            std::reverse(values.begin(), values.end());

            ce.insert({name, values});

            return "inserted in " + name;
        }

        std::string select_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {
            return base_select_or_delete_evaluate(pt, ce, true);
        }

        std::string update_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {

        }

        std::string delete_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {
            return base_select_or_delete_evaluate(pt, ce, false);
        }

        std::string create_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {
            auto name = pt.get<std::string>("name");
            auto list = pt.get_child("list_values").get_child("list");

            std::vector<std::pair<std::string, type>> values;
            BOOST_FOREACH(const auto &v, list) {
                std::string str;
                std::string tp;

                            BOOST_FOREACH(const auto &u, v.second) {
                                auto this_tp = u.second.get<std::string>("<xmlattr>.type");
                                if (this_tp == "column_name") {
                                    str = u.second.get<std::string>("");
                                } else {
                                    tp = u.second.get<std::string>("");
                                }
                            }

                            values.emplace_back(str, type_from_str(tp));
                        }
            std::reverse(values.begin(), values.end());

            ce.create({name, std::move(values)});

            return "created " + name;
        }

        std::string drop_evaluate(const boost::property_tree::ptree &pt, command_executor& ce) {
            ce.drop({ pt.get<std::string>("drop") });
            return "table dropped";
        }

        void print_tree(const boost::property_tree::ptree &pt, int level) {
            const std::string sep(2 * level, ' ');
            BOOST_FOREACH(const auto &v, pt) {
                            std::cout
                                    << sep
                                    << v.first << " : " << v.second.data() << "\n";
                            print_tree(v.second, level + 1);
                        }
        }

    }

    std::string evaluate(std::string& input, command_executor& ce) {

        // std::cout << input << std::endl;

        std::istringstream iss (input);

        xml_schema::properties properties;
        properties.no_namespace_schema_location("../../server/command.xsd");

        try {
            std::unique_ptr<command_t> ptr(command(iss, 0, properties));
            auto maybe_select = ptr->select();
        } catch (xml_schema::exception& e) {
            return e.what();
        }

        boost::property_tree::ptree property_tree;
        std::stringstream stream(input);

        boost::property_tree::read_xml(stream, property_tree);

//        print_tree(property_tree, 0);

        property_tree = property_tree.get_child("command");
        BOOST_FOREACH(auto &v, property_tree) {

            auto command = v.first;
            auto children = property_tree.get_child(command);

            if (command == "create") {
                std::cout << "create query" << std::endl;
                return create_evaluate(children, ce);
            } else if (command == "drop") {
                std::cout << "drop query" << std::endl;
                return drop_evaluate(property_tree, ce);
            } else if (command == "select") {
                std::cout << "select query" << std::endl;
                return select_evaluate(children, ce);
            } else if (command == "insert") {
                std::cout << "insert query" << std::endl;
                return insert_evaluate(children, ce);
            } else if (command == "update") {
                std::cout << "update query" << std::endl;
                return update_evaluate(children, ce);
            } else if (command == "delete") {
                std::cout << "delete query" << std::endl;
                return delete_evaluate(children, ce);
            } else {
                return "error";
            }
        }

    }

};


