#include "visitor.h"

void visit_ast(Ast *root) {
    if (root == nullptr) {
        std::cout << "root == nullptr" << std::endl;
        return;
    }
    std::cout << root->ToString(0) << std::endl;
}

