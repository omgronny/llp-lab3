
%{
    #include "ast.h"
    #include "visitor.h"

    #define YYERROR_VERBOSE 1

    extern int yylex();
    extern int yylineno;
    void yyerror(const char*);
%}

%define parse.error verbose

%token FOR INSERT UPDATE CREATE REMOVE DROP IN FILTER WITH RETURN JOIN OTHER

%token
    END 0       "end of file"

    OPEN_CIRCLE_BRACKET         "("
    OPEN_FIGURE_BRACKET         "{"
    CLOSE_CIRCLE_BRACKET        ")"
    CLOSE_FIGURE_BRACKET        "}"
    ENDLINE     ";"
    COLON     ":"
    COMMA       ","
    DOT         "."
    AND         "&&"
    OR          "||"
;
%token TYPE QUOTE

%token CMP BOOL INT FLOAT STR

%type <intval> CMP
%type <intval> TYPE
%type <boolval> BOOL
%type <intval> INT
%type <floatval> FLOAT
%type <str> STR
%type <intval> AND
%type <intval> OR

// You declare the types of nonterminals using %type.
%type <nonterminal> root
%type <nonterminal> query

%type <nonterminal> select_query
%type <nonterminal> empty_select
%type <nonterminal> filter_select
%type <nonterminal> join_empty_select
%type <nonterminal> join_filter_select

%type <nonterminal> delete_query
%type <nonterminal> empty_delete
%type <nonterminal> filter_delete

%type <nonterminal> update_query
%type <nonterminal> empty_update
%type <nonterminal> filter_update

%type <nonterminal> create_query
%type <nonterminal> drop_query

%type <nonterminal> filter_statement
%type <nonterminal> logic_statement
%type <nonterminal> column
%type <nonterminal> terminal

%type <nonterminal> insert_query
%type <nonterminal> values_list
%type <nonterminal> pair


%union {
    char str[50];
    int intval;
    bool boolval;
    Ast* nonterminal;
    float floatval;

    int cmp_type;
    int logic_op;
    int type;
}

%%

root:
|   root query ENDLINE { visit_ast($2); printf("$> "); }
;

query: select_query
|   insert_query
|   delete_query
|   drop_query
|   update_query
|   drop_query
|   create_query
;

// FOR u IN users FILTER u.status == "active" RETURN u;
// FOR u IN users FILTER u.status == "active" && u.age > 18 return u;
// FOR u IN users FILTER u.status == "active" || u.age > 18 && u.age < 20 return u;
select_query:
|   empty_select
|   filter_select
|   join_empty_select
|   join_filter_select
;
empty_select:
|   FOR STR IN STR RETURN STR { $$ = new_select($4, nullptr, nullptr); }
;
filter_select:
|   FOR STR IN STR FILTER filter_statement RETURN STR { $$ = new_select($4, $6, nullptr); }
;

// FOR u IN users FOR f IN friends RETURN u, f;
join_empty_select:
|   FOR STR IN STR FOR STR IN STR RETURN STR "," STR { $$ = new_select($4, nullptr, new_select($8, nullptr, nullptr)); }
;
// FOR u IN users FOR f IN friends FILTER u.id == f.userId RETURN u, f;
// FOR u IN users FOR f IN friends FILTER u.id == f.userId && f.active == true && (u.status == "active" || f.age > 15) RETURN u, f;
join_filter_select:
|   FOR STR IN STR FOR STR IN STR FILTER filter_statement RETURN STR "," STR { $$ = new_select($4, $10, new_select($8, nullptr, nullptr)); }
;

// FOR u IN users UPDATE u WITH { status: "inactive" } IN users;
// FOR u IN users FILTER u.status == "active" UPDATE u WITH { status: "inactive" } IN users;
update_query:
|   empty_update
|   filter_update
;
empty_update:
|   FOR STR IN STR UPDATE STR WITH "{" values_list "}" IN STR { $$ = new_update($4, nullptr, $9); }
;
filter_update:
|   FOR STR IN STR FILTER filter_statement UPDATE STR WITH "{" values_list "}" IN STR { $$ = new_update($4, $6, $11); }
;

// FOR u IN users FILTER u.status == "active" REMOVE u IN users;
// FOR u IN users FILTER u.status == "active" && u.age > 18 REMOVE u IN users;
// FOR u IN users FILTER u.status == "active" || u.age > 18 && u.age < 20 REMOVE u IN users;
delete_query:
|   empty_delete
|   filter_delete
;
empty_delete:
|   FOR STR IN STR REMOVE STR IN STR { $$ = new_delete($4, nullptr); }
;
filter_delete:
|   FOR STR IN STR FILTER filter_statement REMOVE STR IN STR { $$ = new_delete($4, $6);}
;

// INSERT { firstName: "Anna", name: "Pavlova", profession: "artist" } IN users
insert_query:
|   INSERT "{" values_list "}" IN STR { $$ = new_insert($6, $3); }
;

// CREATE users { ID: INT, active: STR, else: BOOL }
create_query:
|   CREATE STR "{" values_list "}" { $$ = new_create($2, $4); }
;

drop_query:
|   DROP STR { $$ = new_drop($2); }
;

values_list:
|   values_list "," pair { $$ = new_list($3, $1); }
|   pair { $$ = new_list($1, nullptr); }
;

pair:
|   column ":" terminal { $$ = new_pair($1, $3); }
;

// u.status == "not active" && u.active == true
filter_statement:
|   filter_statement "&&" filter_statement { $$ = new_filter($2, $1, $3); }
|   filter_statement "||" filter_statement { $$ = new_filter($2, $1, $3); }
|   "(" filter_statement ")" { $$ = $2; }
|   logic_statement
;

// u.status == "not active"
// u.age > 18
// u.id == f.userId
// vice versa
logic_statement:
|   STR DOT column CMP terminal { $$ = new_compare($4, $3, $5); }
|   terminal CMP STR DOT column { $$ = new_compare(not_cmp($2), $1, $5); }
|   STR DOT column CMP STR DOT column { $$ = new_compare($4, $3, $7); }
;

column:
|   STR { $$ = new_name($1); }
;

terminal:
|   TYPE { $$ = new_type($1); }
|   INT { $$ = new_number($1); }
|   FLOAT { $$ = new_float($1); }
|   BOOL { $$ = new_bool($1); }
|   QUOTE STR QUOTE { $$ = new_string($2); }
;

%left "+" "-";
%left "*" "/" "%";

%left OR;
%left AND;

%%

void yyerror (const char *s) {
   fprintf (stderr, "%s on line number %d", s, yylineno);
}

int main() {
	printf("$> ");
	yyparse();
	return 0;
}



