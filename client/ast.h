#pragma once

#include <memory>
#include <optional>
#include <string>
#include <vector>
#include <iostream>

enum class AstType {
    name,
    string,
    number,
    flt,
    boolean,
    list,
    pair,
    select,
    insert,
    delete_,
    update,
    create,
    drop,
    filter,
    compare,
    type,
};

struct Ast : std::enable_shared_from_this<Ast> {
    AstType type;

    Ast(AstType type);
    virtual std::string ToString(int tabs) = 0;

    virtual ~Ast() = default;
};

template<class AstClass>
inline std::shared_ptr<AstClass> As(Ast* ast);

struct String : public Ast {
    std::string value;

    std::string ToString(int tabs) override;

    String(std::string value);
};

struct ColumnName : public Ast {
    std::string value;

    std::string ToString(int tabs) override;

    ColumnName(std::string value);
};

struct Number : public Ast {
    int value;

    std::string ToString(int tabs) override;

    Number(int value);
};

struct Float : public Ast {
    float value;

    std::string ToString(int tabs) override;

    Float(float value);
};

struct Bool : public Ast {
    bool value;

    std::string ToString(int tabs) override;

    Bool(bool value);
};

enum class data_type {
    STR = 1, INT = 2, FLOAT = 3, BOOL = 4,
};
std::string DataTypeStr(data_type dt);

struct Type : public Ast {
    data_type type;

    std::string ToString(int tabs) override;

    Type(int value);
};

struct List : public Ast {
    std::shared_ptr<Ast> list;
    std::shared_ptr<Ast> next;

    List(std::shared_ptr<Ast> element, std::shared_ptr<Ast> next);

    std::string ToString(int tabs) override;

};

struct Pair : public Ast {
    std::shared_ptr<Ast> left;
    std::shared_ptr<Ast> right;

    std::string ToString(int tabs) override;

    Pair(std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};

struct Select : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> cmp;
    std::shared_ptr<Ast> join;

    std::string ToString(int tabs) override;

    Select(std::string name, std::shared_ptr<Ast> cmp, std::shared_ptr<Ast> join);
};

struct Delete : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> cmp;

    std::string ToString(int tabs) override;

    Delete(std::string name, std::shared_ptr<Ast> cmp);
};

struct Insert : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> list_values;

    std::string ToString(int tabs) override;

    Insert(std::string name, std::shared_ptr<Ast> values);

};

struct Update : public Ast {

    std::string table_name;
    std::shared_ptr<Ast> cmp;
    std::shared_ptr<Ast> values_list;

    std::string ToString(int tabs) override;

    Update(std::string name, std::shared_ptr<Ast> cmp, std::shared_ptr<Ast> values);
};

struct Create : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> list_values;

    std::string ToString(int tabs) override;

    Create(std::string name, std::shared_ptr<Ast> values);

};

struct Drop : public Ast {
    std::string table_name;

    std::string ToString(int tabs) override;

    Drop(std::string name);
};

enum class logic_op {
    AND = 1, OR = 2,
};
std::string LogicOpStr(logic_op lo);


struct Filter : public Ast {
    logic_op operation;
    std::shared_ptr<Ast> left_filter;
    std::shared_ptr<Ast> right_filter;

    std::string ToString(int tabs) override;

    Filter(int operation_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};

enum compare_by {
    GREATER = 1, GREATER_OR_EQUAL = 2, LESS = 3, LESS_OR_EQUAL = 4, EQUAL = 5, NOT_EQUAL = 6, NO_COMPARE = 7,
};
int not_cmp(int cmp);
std::string CompareByStr(compare_by cmp);

struct Compare : public Ast {
    compare_by cmp;
    std::shared_ptr<Ast> left_operand;
    std::shared_ptr<Ast> right_operand;

    std::string ToString(int tabs) override;

    Compare(int cmp_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};


template<class AstClass>
inline std::shared_ptr<AstClass> As(Ast* ast) {
    if (ast == nullptr) {
        return std::shared_ptr<AstClass>(nullptr);
    }
    return std::dynamic_pointer_cast<AstClass>(ast->shared_from_this());
}

template<class AstClass>
inline bool Is(Ast* ast) {
    try {
        auto cast = As<AstClass>(ast);
        return true;
    } catch (...) {
        return false;
    }
}


Ast* new_name(std::string value);
Ast* new_string(std::string value);
Ast* new_number(int value);
Ast* new_float(float value);
Ast* new_bool(bool value);
Ast* new_type(int type);

Ast* new_list(Ast* operand, Ast* prev);
Ast* new_pair(Ast* left, Ast* right);

Ast* new_select(std::string value, Ast* cmp, Ast* join);
Ast* new_delete(std::string value, Ast* cmp);
Ast* new_insert(std::string value, Ast* values);
Ast* new_update(std::string value, Ast* cmp, Ast* values);
Ast* new_create(std::string name, Ast* values);
Ast* new_drop(std::string name);

Ast* new_filter(int operation, Ast* left, Ast* right);
Ast* new_compare(int cmp, Ast* left, Ast* right);

