#include <iostream>
/*
 * insert:
 * INSERT { firstName: "Anna", name: "Pavlova", profession: "artist" } IN users;
 * INSERT { firstName: "Anna", name: "Pavlova" } IN users;
 * INSERT { firstName: "Anna" } IN users;
 *
 * delete:
 * FOR u IN users FILTER u.status == "deleted" REMOVE u IN users
 *
 * update:
 * FOR u IN users FILTER u.status == "active" UPDATE u WITH { status: "inactive" } IN users
 *
 * select:
 * FOR u IN users FILTER u.status == "active" && u.active == true RETURN u;
 * FOR u IN users FILTER u.age > 18 RETURN u;
 * FOR u IN users FILTER 18 > u.age RETURN u;
 * FOR u IN users RETURN u;
 *
 * join:
 * FOR u IN users FOR f IN friends FILTER u.id == f.userId RETURN u, f;
 *
 * create:
 * CREATE users { ID: INT, active: STR, else: BOOL }
 *
 * drop:
 * DROP users;

 CREATE table { col1: INT, col2: STR };
 FOR u in table RETURN u;
 INSERT { col1: 2, col2: "test2" } IN table;
 FOR u IN table FILTER u.col2 == "test1" REMOVE u IN table;

 INSERT { col1: 100500, col2: "new" } IN new_test2;
 FOR u in new_test2 RETURN u;
 FOR u in new_test2 FILTER u.col1 > 7 RETURN u;
 FOR u IN new_test2 FILTER u.col1 > 100 REMOVE u IN new_test2;
 INSERT { id: 126 } IN jointest;
 FOR u in jointest RETURN u;
 FOR u IN new_test2 FOR f IN jointest FILTER u.col1 == f.id RETURN u, f;
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ast.h"
#include "parser.tab.h"
#include "xml_parser.h"

int main() {

    Ast* this_root;

    std::string inp;

    int32_t sock = socket(AF_INET, SOCK_STREAM, 0);
    // < 0 error

    sockaddr_in addr{};
    addr.sin_family = AF_INET;
    addr.sin_port = htons(8187);
    // addr.sin_addr.s_addr = inet_addr(IP);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if (connect(sock, reinterpret_cast<sockaddr*>(&addr), sizeof (addr)) < 0) {
        // error;
        std::cout << "connect error" << std::endl;
        return -1;
    }

    char buf[1024 * 100];

    while (true) {
        printf("$> ");
        this_root = yyparse();
        // приводим в xml
        auto xml_string = xml_parser::ast_to_xml(this_root);
        // std::cout << xml_string << std::endl;
        // TODO: отправляем

        char* str = xml_string.data();

        send(sock, str, xml_string.size(), 0);
        auto response_size = recv(sock, buf, 1024 * 100, 0);

        if (response_size == -1) {
            return -1;
        }

        buf[response_size] = 0;

        std::cout << buf << std::endl;

    }
    return 0;
}

