#include "ast.h"
#include <memory>

Ast::Ast(AstType type)
        : type(type) {}

String::String(std::string value)
        : Ast(AstType::string), value(std::move(value)) {}

ColumnName::ColumnName(std::string value)
        : Ast(AstType::name), value(std::move(value)) {}

Number::Number(int value)
        : Ast(AstType::number), value(value) {}

Float::Float(float value)
        : Ast(AstType::flt), value(value) {}

Bool::Bool(bool value)
        : Ast(AstType::boolean), value(value) {}

Type::Type(int value)
        : Ast(AstType::type)
        , type(static_cast<data_type>(value))
{}

List::List(std::shared_ptr<Ast> element, std::shared_ptr<Ast> next)
        : Ast(AstType::list), list(std::move(element)), next(std::move(next)) {}

Pair::Pair(std::shared_ptr<Ast> left, std::shared_ptr<Ast> right)
    : Ast(AstType::pair)
    , left(std::move(left))
    , right(std::move(right)) {}

Select::Select(std::string name, std::shared_ptr<Ast> cmp_, std::shared_ptr<Ast> join_)
        : Ast(AstType::select)
        , table_name(std::move(name))
        , cmp(std::move(cmp_))
        , join(std::move(join_))
{}

Delete::Delete(std::string name, std::shared_ptr<Ast> cmp)
        : Ast(AstType::delete_)
        , table_name(std::move(name))
        , cmp(std::move(cmp))
{}

Insert::Insert(std::string name, std::shared_ptr<Ast> values)
    : Ast(AstType::insert)
    , table_name(std::move(name))
    , list_values(std::move(values))
{}

Update::Update(std::string name, std::shared_ptr<Ast> cmp, std::shared_ptr<Ast> values)
        : Ast(AstType::update)
        , table_name(std::move(name))
        , cmp(std::move(cmp))
        , values_list(std::move(values))
{}

Create::Create(std::string name, std::shared_ptr<Ast> values)
    : Ast(AstType::create)
    , table_name(std::move(name))
    , list_values(std::move(values))
{}

Drop::Drop(std::string name)
    : Ast(AstType::drop)
    , table_name(std::move(name))
{}

Filter::Filter(int operation_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right)
    : Ast(AstType::filter)
    , operation(static_cast<logic_op>(operation_number))
    , left_filter(std::move(left))
    , right_filter(std::move(right))
{}

Compare::Compare(int cmp_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right)
        : Ast(AstType::compare)
        , cmp(static_cast<compare_by>(cmp_number))
        , left_operand(std::move(left))
        , right_operand(std::move(right))
{}

std::string DataTypeStr(data_type dt) {
    switch (dt) {
        case data_type::STR:
            return "STR";
        case data_type::INT:
            return "INT";
        case data_type::FLOAT:
            return "FLOAT";
        case data_type::BOOL:
            return "BOOL";
    }
}

std::string LogicOpStr(logic_op lo) {
    switch (lo) {
        case logic_op::AND:
            return "AND";
        case logic_op::OR:
            return "OR";
    }
}

int not_cmp(int cmp) {
    switch (cmp) {
        case GREATER:
            return LESS;
        case GREATER_OR_EQUAL:
            return LESS_OR_EQUAL;
        case LESS:
            return GREATER;
        case LESS_OR_EQUAL:
            return GREATER_OR_EQUAL;
        case EQUAL:
            return EQUAL;
        case NOT_EQUAL:
            return NOT_EQUAL;
        case NO_COMPARE:
            return NO_COMPARE;
    }
}

std::string CompareByStr(compare_by cmp) {
    switch (cmp) {
        case GREATER:
            return "GREATER";
        case GREATER_OR_EQUAL:
            return "GREATER_OR_EQUAL";
        case LESS:
            return "LESS";
        case LESS_OR_EQUAL:
            return "LESS_OR_EQUAL";
        case EQUAL:
            return "EQUAL";
        case NOT_EQUAL:
            return "NOT_EQUAL";
        case NO_COMPARE:
            return "NO_COMPARE";
    }
}

Ast* new_string(std::string value) {
    return new String(std::move(value));
}
Ast* new_name(std::string value) {
    return new ColumnName(std::move(value));
}
Ast* new_number(int value) {
    return new Number(value);
}
Ast* new_float(float value) {
    return new Float(value);
}
Ast* new_bool(bool value) {
    return new Bool(value);
}
Ast* new_type(int type) {
    return new Type(type);
}
Ast* new_list(Ast* operand, Ast* prev) {
    return new List(std::shared_ptr<Ast>(operand), std::shared_ptr<Ast>(prev));
}
Ast* new_pair(Ast* left, Ast* right) {
    return new Pair(std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
Ast* new_select(std::string name, Ast* cmp, Ast* join) {
    return new Select(std::move(name), std::shared_ptr<Ast>(cmp), std::shared_ptr<Ast>(join));
}
Ast* new_delete(std::string name, Ast* cmp) {
    return new Delete(std::move(name), std::shared_ptr<Ast>(cmp));
}
Ast* new_insert(std::string name, Ast* values) {
    return new Insert(std::move(name), std::shared_ptr<Ast>(values));
}
Ast* new_update(std::string name, Ast* cmp, Ast* values) {
    return new Update(std::move(name), std::shared_ptr<Ast>(cmp), std::shared_ptr<Ast>(values));
}
Ast* new_create(std::string name, Ast* values) {
    return new Create(std::move(name), std::shared_ptr<Ast>(values));
}
Ast* new_drop(std::string name) {
    return new Drop(std::move(name));
}
Ast* new_filter(int operation, Ast* left, Ast* right) {
    return new Filter(operation, std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
Ast* new_compare(int cmp, Ast* left, Ast* right) {
    return new Compare(cmp, std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
