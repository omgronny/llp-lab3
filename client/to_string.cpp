#include "ast.h"

static const std::string tab = " ";

std::string String::ToString(int tabs) {
    std::string res;
    return res + "\"string\">" + value;
}

std::string ColumnName::ToString(int tabs) {
    std::string res;
    return res + "\"column_name\">" + value;
}

std::string Number::ToString(int tabs) {
    std::string res;
    return res + "\"number\">" + std::to_string(value);
}


std::string Float::ToString(int tabs) {
    std::string res;
    return res + "\"float\">" + std::to_string(value);
}


std::string Bool::ToString(int tabs) {
    std::string res;
    return res + "\"bool\">" + std::to_string(value);
}


std::string Type::ToString(int tabs) {
    std::string res;
    return res + "\"type\">" + DataTypeStr(type);
}


std::string List::ToString(int tabs) {
    std::string res;
    // std::cout << "list" << std::endl;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<list>\n";
    auto item = shared_from_this();
    while (item != nullptr) {
        res += As<List>(item.get())->list->ToString(tabs + 1) + "\n";
        item = As<List>(item.get())->next;
    }
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</list>";
}

std::string Pair::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<pair>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<value type=" + left->ToString(tabs + 1) + "</value>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<value type=" + right->ToString(tabs + 1) + "</value>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</pair>";
}


std::string Select::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += " ";
    }
    res += "<select>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<table>" + table_name + "</table>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<cmp>";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "<nullable>nullptr</nullable>";
    }
    res += "</cmp>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<join>";
    if (join != nullptr) {
        res += "\n";
        res += join->ToString(tabs + 2);
    } else {
        res += "<nullable>nullptr</nullable>";
    }
    res += "</join>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</select>";
}


std::string Delete::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<delete>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<table>" + table_name + "</table>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<cmp>";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "</cmp>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</delete>";
}

std::string Insert::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<insert>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<name>" + table_name + "</name>\n" + list_values->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</insert>";
}


std::string Update::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<update>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<table>" + table_name + "</table>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<cmp>";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "</cmp>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<list_values>\n" + values_list->ToString(tabs + 2) + "\n</list_values>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</update>";
}

std::string Create::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<create>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<name>" + table_name + "</name>\n<list_values>" + list_values->ToString(tabs + 1) + "</list_values>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</create>";
}

std::string Drop::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<drop>" + table_name + "</drop>";

    return res;
}

std::string Filter::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<filter><left_filter>\n" + left_filter->ToString(tabs + 1) + "</left_filter>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<operation>" + LogicOpStr(operation) + "</operation>\n<right_filter>" + right_filter->ToString(tabs + 1) + "</right_filter>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</filter>";
}


std::string Compare::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<compare><left_operand type=" + left_operand->ToString(tabs + 1) + "</left_operand>\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "<compare_by>" + CompareByStr(cmp) + "</compare_by"">\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "<right_operand type=" + right_operand->ToString(tabs + 1) + "</right_operand>\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "</compare>";
}


