**Глинских Роман P33102**

# Низкоуровневое Программирование. Лабораторная работа 3

## Installation and start

### Linux:

##### Requirements:
- cmake (3.21)
- make
- C++20 compiler (Clang 14 or newer, gcc 10 or newer)

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab2.git
cd llp-lab3
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make all
```

- Server
    ```
  make server
  ./server/server
  ```
- Client:
    ```
  make client
  ./client/client
  ```

### Windows:

- cmake (3.21)
- make
- GCC 12.2.0 + MinGW-w64 10.0.0 (you can download it from [here](https://winlibs.com/))

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab2.git
cd llp-lab3
md build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
```

- Server
    ```
  make server
  ./server/server <filename>
  ```
- Client: 
    ```
  make client
  ./client/client
  ```
(hint: Отедльно про сервеную часть читать [тут](https://gitlab.se.ifmo.ru/omgronny/llp-lab1), про клиентскую [тут](https://gitlab.se.ifmo.ru/omgronny/llp-lab2))

## Цель задания
На базе данного транспортного формата описать схему протокола обмена информацией и воспользоваться
существующей библиотекой по выбору для реализации модуля, обеспечивающего его функционирование.
Протокол должен включать представление информации о командах создания, выборки, модификации и
удаления данных в соответствии с данной формой, и результатах их выполнения.

## Задачи

1. Изучить выбранную библиотеку
2. На основе существующей библиотеки реализовать модуль, обеспечивающий взаимодействие
3. Реализовать серверную часть в виде консольного приложения
4. Реализовать клиентскую часть в виде консольного приложения

## Описание работы и реализации

- В качестве библиотеки была выбрана `XSD Synthesis Lab`
  - Поддерживает кодогенерацию в соответствии со схемой ([server/command.xsd](server/command.xsd))
- Реализация включает асинхронное `TCP` взаимодействие. 
  - В качестве асинхронного фреймворка был выбран `boost-asio` и `c++20 coroutines`. 
  - Код взаимодействия находится в [server/main.cpp](server/main.cpp).
- Основная работу (парсинг `aql` запроса в виде `xml` ввода) происходит в модуле [server/eval.cpp](server/eval.h).
  - В начале происходит валидация файла в соответствии со схемой (точнее, с кодом, который был по ней сгенерирован)
  - Если запрос валидидируется, происходит его парсинг и выполнение (вызывается модуль, реализованный в лабораторной номер 1)
  - Если запрос неверный, возвращается исключение

## Результаты

Пример клиент-серверной сессии (клиент отправляет запросы и получает ответы)

```
[omgronny@omgronny build]$ ./client/client
$> FOR u in new_test2 RETURN u;
new_test2;229;testtesttest
new_test2;228;test11
new_test2;10;test10
new_test2;3;test3
new_test2;2;test2
new_test2;1;test1

$> FOR u in new_test2 FILTER u.col1 > 7 RETURN u;
new_test2;229;testtesttest
new_test2;228;test11
new_test2;10;test10

$> INSERT { col1: 100500, col2: "new" } IN new_test2;
inserted in new_test2
$> FOR u in new_test2 FILTER u.col1 > 7 RETURN u;
new_test2;100500;new
new_test2;229;testtesttest
new_test2;228;test11
new_test2;10;test10

$> FOR u IN new_test2 FILTER u.col1 > 100 REMOVE u IN new_test2;
deleted
$> FOR u in new_test2 RETURN u;
new_test2;10;test10
new_test2;3;test3
new_test2;2;test2
new_test2;1;test1

$> CREATE table { col1: INT, col2: STR };
created table
$> INSERT { col1: 1, col2: "test1" } IN table;
inserted in table
$> FOR u in table RETURN u;
table;1;test1

$> INSERT { col1: 2, col2: "test2" } IN table;
inserted in table
$> FOR u in table RETURN u;
table;2;test2
table;1;test1
$> FOR u IN table FILTER u.col2 == "test1" REMOVE u IN table;
deleted
$> FOR u in table RETURN u;
table;2;test2
```

(Лог сервера во время сессии)

```
listener
new client
select query
new_test2
select query
new_test2 col1 0 7
insert query
select query
new_test2 col1 0 7
delete query
new_test2 col1 0 100
select query
new_test2
create query
insert query
select query
table
insert query
select query
delete query
table col2 4 test1
select query
table
```

XML-схема для передачи данных:
```xml
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xs:complexType name="variant">
        <xs:simpleContent>
            <xs:extension base="xs:string">
                <xs:attribute name="type" type="xs:string" />
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>

    <xs:complexType name="compare_t">
        <xs:sequence>
            <xs:element name="left_operand" type="variant" />
            <xs:element name="compare_by" type="xs:string"/>
            <xs:element name="right_operand" type="variant"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="cmp_t">
        <xs:choice>
            <xs:element name="compare" type="compare_t"/>
            <xs:element name="nullable" type="xs:string"/>
        </xs:choice>
    </xs:complexType>

    <xs:complexType name="join_t">
        <xs:choice>
            <xs:element name="nullable" type="xs:string"/>
            <xs:element name="select" type="select_t"/>
        </xs:choice>
    </xs:complexType>

    <xs:complexType name="select_t">
        <xs:sequence>
            <xs:element name="table" type="xs:string"/>
            <xs:element name="cmp" type="cmp_t"/>
            <xs:element name="join" type="join_t"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="delete_t">
        <xs:sequence>
            <xs:element name="table" type="xs:string"/>
            <xs:element name="cmp" type="cmp_t"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="list_t">
        <xs:sequence>
            <xs:element name="pair" minOccurs="1" maxOccurs="50">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="value" type="variant" minOccurs="2" maxOccurs="2"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="insert_t">
        <xs:sequence>
            <xs:element name="name" type="xs:string"/>
            <xs:element name="list" type="list_t"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="create_t">
        <xs:sequence>
            <xs:element name="name" type="xs:string"/>
            <xs:element name="list_values">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="list" type="list_t"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="command_t">
        <xs:choice>
            <xs:element name="select" type="select_t"/>
            <xs:element name="delete" type="delete_t"/>
            <xs:element name="insert" type="insert_t"/>
            <xs:element name="create" type="create_t"/>
            <xs:element name="drop" type="xs:string"/>
        </xs:choice>
    </xs:complexType>

    <xs:element name="command" type="command_t" />

</xs:schema>
```

## Вывод

В процессе выполнения лабораторной работы я познакомился с понятием xml-схемы и научился применять ее для генерации парсеров.
Также я поближе познакомился с сетевым взаимодействием

