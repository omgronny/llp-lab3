
project(llp-lab3)

cmake_minimum_required(VERSION 3.21)

set(CMAKE_CXX_STANDARD             20)

add_subdirectory(client)
add_subdirectory(server)

